import json

from uuid import uuid1
from kafka import SimpleConsumer, SimpleClient, SimpleProducer


class JungTasker:

    def __init__(self, in_topic, out_topic, ip="localhost", port="9092",
                 group="my-group"):
        self.client = SimpleClient(ip + ':' + port)
        self._in_topic = in_topic
        self.in_topic = in_topic
        self._out_topic = out_topic
        self.out_topic = out_topic
        self.group = group
        self.producer = SimpleProducer(self.client, async_send=False)
        self.consumer = SimpleConsumer(self.client, group, self.in_topic,
                                       auto_commit=False)
        self.reset_partitions()

    def set_topics(self, in_topic=None, out_topic=None):
        if in_topic is not None:
            self.in_topic = in_topic
            self.consumer = SimpleConsumer(self.client, self.group, self.in_topic,
                                           auto_commit=False)
        if out_topic is not None:
            self.out_topic = out_topic
        self.reset_partitions()

    def reset_topics(self):
        self.set_topics(self._in_topic, self._out_topic)

    def reset_partitions(self):
        for partition in self.consumer.offsets:
            self.consumer.reset_partition_offset(partition)

    def create_task(self, task, content):
        new_task = {"id": str(uuid1()), "task": task, "content": content}
        return new_task

    def publish(self, json_message):
        message = json.dumps(json_message)
        self.producer.send_messages(self.out_topic, message.encode())

    def get_task_result(self, task_id, retries=20, block_time=0.03):
        while retries > 0:
            retries -= 1
            for offset in self.consumer.get_messages(count=1,
                                                     block=True,
                                                     timeout=block_time):
                message = json.loads(offset.message.value.decode('utf-8'))
                if "task_id" in message and message["task_id"] == task_id:
                    return message["result"]
        return None
