import json

from .jung_tasker import JungTasker


class JungRegistry:

    def __init__(self, kafka_broker, in_topic, out_topic, port, group):
        self.storage = {}
        self.kafka = kafka_broker
        self.port = port
        self.tasker = JungTasker(in_topic=in_topic, out_topic=out_topic,
                                 ip=kafka_broker, port=port, group=group)

        print("kafka broker: {}".format(self.kafka))
        print("starting {}...".format(group))

    def process_message(self, message):
        pass

    def process_tasks(self):
        print("waiting for messages...")
        while True:
            for offset_message in self.tasker.consumer.get_messages(count=1,
                                                                    block=True,
                                                                    timeout=0.03):
                message = json.loads(offset_message.message.value)
                try:
                    response = self.process_message(message)
                except Exception:
                    response = {"error": "malformed message"}

                self.tasker.publish(response)
